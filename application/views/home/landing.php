
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#" style="font-size: 28px;"><b>NgoBang</b></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse d-flex justify-content-end me-3" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active me-4" aria-current="page" href="#" style="font-family: sofia-sans; font: size 24px;"><b>Beranda</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link fw-bold me-4" href="#" style="font-family: sofia-sans; font: size 24px;">Kursus</a>
              </li>
              <li class="nav-item">
                <a class="nav-link me-4" href="#" style="font-family: sofia-sans; font: size 24px;"><b>Lainnya</b></a>
              </li>
              <li class="nav-item">
                <a class="btn btn-primary text-white" href="<?= base_url('auth');?>" target="_blank" role="button">Login</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <!-- End navbar -->
      
    <!-- Hero unit -->
    <div class="container-fluid py-4 " >
      <div class="container">
        <div class="row">
          <div class="col-6">
            <h1 class="h1-website">Website Belajar Coding Bahasa Indonesia</h1>
            <p lclass="p-hero">Menjadi seorang programmer handal dan berkualitas untuk menghadapi era teknologi yang semakin berkembang</p>
            <button class="button-kursus btn btn-lg text-white">Lihat Kursus</button>
          </div>

          <div class="col-5 offset-1">
            <img src="<?= base_url('assets/');?>images/4880440 1.png" alt="" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
    <!-- End hero Unit -->
    <!-- Kenapa harus kami -->
    <div class="container-fluid py-5">
      <div class="container py-5">
        <div class="row">
          <div class="col-12 text-center">
            <h2>Kenapa harus memilih kami?</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg " style="width: 18rem;">
              <img src="<?= base_url('assets/');?>images/dollar-iso-color.png" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text text-center"><b>Harga Terjangkau</b></p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg" style="width: 18rem;">
              <img src="<?= base_url('assets/');?>images/play-iso-color (1).png" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text text-center"><b>Akses dimana saja</b></p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg" style="width: 18rem;">
              <img src="<?= base_url('assets/');?>images/boy-iso-color (1).png" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text text-center"><b>Mentor berpengalaman</b></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End kenapa harus kami -->

    <!-- Pilihan Kursus -->
    <div class="container-fluid py-5" >
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2>Paket Kursus Rekomendasi Kami</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg rounded-top" style="width: 18rem;">
              <img src="<?= base_url('assets/');?>images/JavaScript.png" class="card-img-top center" alt="..." >
              <div class="card-body text-center">
                <h5 class="card-title">Mahir JavaScript</h5>
                <p class="card-text ">Rp.99.000</p>
                <a href="#" class="btn btn-primary">Beli sekarang</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg rounded-top" style="width: 20rem;">
              <img src="<?= base_url('assets/');?>images/Php.png" class="card-img-top center" alt="...">
              <div class="card-body text-center">
                <h5 class="card-title">Mahir PHP</h5>
                <p class="card-text ">Rp.99.000</p>
                <a href="#" class="btn btn-primary">Beli sekarang</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-4 py-5">
            <div class="card shadow-lg rounded-top" style="width: 20rem;">
              <img src="<?= base_url('assets/');?>images/Java.png" class="card-img-top center" alt="...">
              <div class="card-body text-center">
                <h5 class="card-title">Mahir Java</h5>
                <p class="card-text ">Rp.99.000</p>
                <a href="#" class="btn btn-primary">Beli sekarang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End pilihan kursus -->
