<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href=" <?= base_url('assets/'); ?>bootstrap-5.2.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href=" <?= base_url('assets/'); ?>bootstrap-5.2.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href=" <?= base_url('assets/'); ?>styles.css">
    <title><?= $title ?></title>
</head>
<body>