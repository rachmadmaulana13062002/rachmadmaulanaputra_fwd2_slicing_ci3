    <div class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <img src="<?= base_url('assets/'); ?>images/Tablet login-cuate.png" alt="" class="img-fluid">
                </div>

                <div class="col-5 offset-1">
                    <h1 class="mt-5 text-center">LOGIN</h1>
                    <p class="py-3 text-center" style="color:574A4A;">Login untuk mengakses kelas</p>
                    <form>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Email address</label>
                          <input type="email" class="form-control" placeholder="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputPassword1" class="form-label">Password</label>
                          <input type="password" class="form-control" placeholder="password" id="exampleInputPassword1">
                        </div>
                        <button type="submit" class="btn btn-primary w-100 shadow-lg">Login</button>
                      </form>
                </div>
            </div>
        </div>
    </div>