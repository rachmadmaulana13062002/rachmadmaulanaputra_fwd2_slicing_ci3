<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LandingPage extends CI_Controller {
    public function index(){
       $data['title'] = 'Landing Page'; 
       $this->load->view('templates/auth_header',$data);
       $this->load->view('home/landing');
       $this->load->view('templates/auth_footer');
    }
}